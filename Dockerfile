FROM python:3.10
LABEL MAINTAINER="Mohammad Mahdi Mohammadi Fath|mohammad74mf@gmail.com|mohammad74mf@yahoo.com"
ENV PYTHONUNBUFFERED 1

# Set working directory
RUN mkdir /fanavaranapp
WORKDIR /fanavaranapp
COPY . /fanavaran

# Installing requirements
ADD requirements.txt /fanavaranapp
RUN pip install --upgrade pip
RUN pip install -r requirements.txt


# Collect static files
#RUN python manage.py collectstatic --no-input 

#CMD ["gunicorn", "--chdir", "/pharmaapp", "--bind", ":8000", "pharmaapp.wsgi:application"]
CMD ["python","manage.py","runserver","0.0.0.0:80"]


