from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken

from web.models import Topic, Auther, Book, Loan, CustomUser


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    default_error_messages = {
        "detail": ("No active account found with the given credentials")
    }

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        token["first_name"] = user.first_name
        token["last_name"] = user.last_name
        token["role"] = user.role

        return token


class UserProfileSerializer(serializers.ModelSerializer):
    """Serializers a user object"""

    class Meta:
        model = CustomUser
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "password",
        )
        extra_kwarg = {
            "passworod": {"write_only": True, "style": {"input_type": "password"}}
        }

    def create(self, validated_data):
        """Create and return a new user"""

        user = CustomUser.objects.create_user(**validated_data)
        return user


class TopicSerializer(serializers.ModelSerializer):
    """Serializer"""

    class Meta:
        model = Topic
        fields = "__all__"


class AutherSerializer(serializers.ModelSerializer):
    """Serializer"""

    class Meta:
        model = Auther
        fields = "__all__"

class BookSerializer(serializers.ModelSerializer):
    """Serializer"""
    topic_name = serializers.ReadOnlyField(source="topic.topic_name")
    auther_name = serializers.ReadOnlyField(source="auther.auther_name")
    class Meta:
        model = Book
        fields = "__all__"


class LoanSerializer(serializers.ModelSerializer):
    """Serializer"""
    book_name = serializers.ReadOnlyField(source="book_id.title")
    topic_name = serializers.ReadOnlyField(source="book_id.topic.topic_name")
    auther_name = serializers.ReadOnlyField(source="book_id.auther.auther_name")
    user_first_name = serializers.ReadOnlyField(source="user_id.first_name")
    user_last_name = serializers.ReadOnlyField(source="user_id.last_name")
    class Meta:
        model = Loan
        fields = "__all__"

    def create(self, validated_data):
        book = validated_data["book_id"]
        Book.objects.filter(id=book.id).update(is_loan=True)
        return Loan.objects.create(**validated_data)