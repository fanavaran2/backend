from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from api import views

schema_view = get_schema_view(
   openapi.Info(
      title="Library API",
      default_version='v1',
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.register("register", views.UserViewSet, basename="register"),
router.register("topic", views.TopicViewSet, basename="topic"),
router.register("auther", views.AutherViewSet, basename="auther"),
router.register("book", views.BookViewSet, basename="book"),
router.register("loan", views.LoanViewSet, basename="loan"),

urlpatterns = [
    path("", include(router.urls)),
    path('loan-list/', views.LoanListView.as_view(), name='loan-list'),
    path('token/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
