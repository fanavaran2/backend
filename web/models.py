from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models


from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):

    ROLE = (
        (1, "User"),
        (2, "LIBRARIAN"),
    )
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    role = models.CharField(
        max_length=1, choices=ROLE, null=True, blank=True, default=1
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ["first_name", "last_name",]

    objects = CustomUserManager()

    def __str__(self):
        return "%s : %s" % (self.last_name, self.first_name)

class Topic(models.Model):
    """Database model for topic"""

    topic_name = models.CharField(max_length=50)
    

    def __str__(self):
        return self.topic_name


class Auther(models.Model):
    """Database model for auther"""

    auther_name = models.CharField(max_length=50)
    

    def __str__(self):
        return self.auther_name


class Book(models.Model):
    """Database model for book"""

    title = models.CharField(max_length=50)
    topic = models.ForeignKey(
        Topic,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    auther = models.ForeignKey(
        Auther,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    isbn = models.CharField(max_length=13, unique=True)
    is_loan = models.BooleanField(default=False)

    
    def __str__(self):
        return self.title


class Loan(models.Model):
    """Database model for loan"""

    user_id = models.ForeignKey(
        CustomUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    book_id = models.ForeignKey(
        Book,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    loan_date_at = models.DateField(auto_now_add=True)
    return_date_at = models.DateField(null=True, blank=True)


    def __str__(self):
        return "%s : %s" % (" کاربر - ", self.book_id.title)