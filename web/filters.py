import django_filters
from .models import Book

class BookFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(label='title', lookup_expr='icontains')
    topic__topic_name = django_filters.CharFilter(label='topic name', lookup_expr='icontains')
    auther__auther_name = django_filters.CharFilter(label='auther name',lookup_expr='icontains')

    class Meta:
        model = Book
        fields = ['topic', 'auther', ]