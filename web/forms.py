from django.contrib.auth.forms import UserCreationForm
from django import forms

from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):

    email = forms.EmailField(required=True, widget= forms.TextInput
                           (attrs={'class':'form-control',
				   'id':'email'}))
    first_name = forms.CharField(max_length=50, required=True, widget= forms.TextInput
                           (attrs={'class':'form-control',
				   'id':'first_name'}))
    last_name = forms.CharField(max_length=50, required=True, widget= forms.TextInput
                           (attrs={'class':'form-control',
				   'id':'last_name'}))
    password1 = forms.CharField(required=True, widget= forms.PasswordInput
                           (attrs={'class':'form-control',
				   'id':'password1'}))
    password2 = forms.CharField(required=True, widget= forms.PasswordInput
                           (attrs={'class':'form-control',
				   'id':'password2'}))
    class Meta:
        model = CustomUser
        fields = ("email", "first_name", "last_name", "password1", "password2")