from django.contrib import admin

# Register your models here.
from web import models


@admin.register(models.Topic)
class CustomersAdmin(admin.ModelAdmin):
    list_display = ("topic_name",)
    search_fields = ("topic_name",)


@admin.register(models.Auther)
class CustomersAdmin(admin.ModelAdmin):
    list_display = ("auther_name",)
    search_fields = ("auther_name",)


@admin.register(models.Book)
class CustomersAdmin(admin.ModelAdmin):
    list_display = ("title", "topic", "auther",)
    search_fields = ("title",)


@admin.register(models.Loan)
class CustomersAdmin(admin.ModelAdmin):
    list_display = ("user_id", "book_id", "loan_date_at", "return_date_at")
    search_fields = ("book_id__title",)



@admin.register(models.CustomUser)
class CustomersAdmin(admin.ModelAdmin):
    list_display = ("email", "first_name", "last_name")
    search_fields = ("email",)