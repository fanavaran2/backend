from django.shortcuts import render, redirect
from .forms import CustomUserCreationForm
from .models import Book
from .filters import BookFilter



# Create your views here.

def home(request):
    books = Book.objects.all()
    book_filter = BookFilter(request.GET, queryset=books)
    return render(request, 'index.html', {'filter': book_filter})



def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = CustomUserCreationForm()

    context = {'form': form}
    return render(request, 'register.html', context)
