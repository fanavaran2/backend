# Fanavaran library

## ER Diagram
![er diagram](./doc/er.png)

## Demo

<http://37.32.12.240:8000/admin/>

<http://37.32.12.240:8000/login/>

Email : mohammad74mf@gmail.com 

Password : 123456

## Swagger
<http://37.32.12.240:8000/api/swagger/>

![swagger](./doc/swagger.png)


## How to run
- clone repository
```
git clone https://gitlab.com/fanavaran2/backend.git
```

- update vue frontend submodule
```
git submodule init
git submodule update --recursive
git submodule update --remote
```

- set enviroments variable
```
cp .env.example library/.env
```

- edit library/.env and set enviroments variable with your database config

```
DEBUG=True
DB_NAME=mydb
DB_USER=myuser
DB_PASS=mypass
DB_HOST=localhost
DB_PORT=5432
```

- deploy with docker compose
```
docker compose up
```

- create db and user in container postgreSQL
```
docker exec -it fanavaran-postgresql psql --user "postgres"
```

and then in psql shell create db, user and grant privileges
```
postgres=# create database mydb;
postgres=# create user myuser with encrypted password 'mypass';
postgres=# grant all privileges on database mydb to myuser;
```
exit psql shell with
```
ctrl+d
```

- down container and up again
```
docker compose down
docker compose up
```
- run migration commands
```
docker exec -it fanavaran-back python manage.py makemigrations
docker exec -it fanavaran-back python manage.py migrate
```

## Create superuser
```
docker exec -it fanavaran-back python manage.py createsuperuser
```

## Routes
Vue frontend
- <http://localhost:8001/login>

Django template 
- <http://localhost:8000/>

Django admin
- <http://localhost:8000/admin>

API swagger

- <http://localhost:8000/api/swagger/>
